package moodle;

import java.awt.*;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;

public class Main {
    public static void main(String[] args) {
        Console console = System.console();
        if (console == null && !GraphicsEnvironment.isHeadless()) {
            String filename = Main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
            try {
                if (System.getProperty("os.name").contains("Windows")) {
                    Runtime.getRuntime().exec(new String[]{"cmd", "/c", "start", "cmd", "/k", "java -jar \"" + filename + "\""});
                } else {
                    Runtime.getRuntime().exec("java -jar \"" + filename + "\"");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Main.run();
            System.out.println("Program has ended, please type 'exit' to close the console");
        }
    }

    public static void run() {
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
        String username, password;
        while (true) {
            try {
                System.out.println("Please enter your moodle username: ");
                username = bfr.readLine();
                System.out.println("Please enter your moodle password: ");
                password = bfr.readLine();
                if (Caller.call(username, password)) {
                    try {
                        if (System.getProperty("os.name").contains("Windows")) {
                            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                        } else {
                            Runtime.getRuntime().exec("clear");
                        }
                    } catch (InterruptedException ignore) {
                    }
                    System.out.println("Login successfully! You will be auto logged every 5th minute!");
                    break;
                }
                System.out.println("Invalid credentials, please try again!");
            } catch (IOException e) {
                System.out.println("Something broke, please try again!");
                e.printStackTrace();
            }
        }

        while (true) {
            try {
                Calendar now = Calendar.getInstance();
                if (now.get(Calendar.MINUTE) % 5 == 0) {
                    Caller.call(username, password);
                }
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
