package moodle;


import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Caller {
    private static final CloseableHttpClient CLIENT = HttpClientBuilder.create().build();
    private static final String MOODLE_URL = "https://fpmi.bg/moodle/login/index.php";
    private static final String MOODLE_LOGOUT_URL = "https://fpmi.bg/moodle/login/logout.php?sesskey=";
    private static String lastSessKey = null;

    static boolean call(String username, String password){
        HttpPost request = new HttpPost(MOODLE_URL);
        if (lastSessKey != null){
            logout();
        }
        try {
            ArrayList<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("rememberusername", "1"));

            request.setEntity(new UrlEncodedFormEntity(params));
            request.setHeader("content-type", "application/x-www-form-urlencoded");
            CloseableHttpResponse response = CLIENT.execute(request);
            String entity = EntityUtils.toString(response.getEntity());
            if (entity.contains("You are not logged in")) {
                return false;
            }

            final Pattern pattern = Pattern.compile("\"sesskey\":\"([A-Za-z0-9]{10})\"", Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(entity);
            while (matcher.find()) {
                lastSessKey = matcher.group(1);
            }

            return lastSessKey != null;
        } catch (IOException e) {
            System.out.println("Error happened, request canceled: " + e.getMessage());
            return false;
        }
    }

    private static void logout(){
        HttpGet request = new HttpGet(MOODLE_LOGOUT_URL + lastSessKey);
        lastSessKey = null;
        try {
            CLIENT.execute(request);
        } catch (IOException e) {
            System.out.println("Error logging out...");
        }

    }
}
